const request = require("supertest");
const { setupStrapi } = require("../helpers/strapi");

it("should send email", async (done) => {
  let { sendMail } = require("../../api/categoria//services/categoria")

  expect(await sendMail('jcabrera@estratek.com', 'Reservi - Restablece tu contraseña', `<p>Ingresar al siguiente link para restablecer tu contraseña <a href="http://localhost:4200/forgot-password?t=${encodeURI('asdjkha98a823eguybo%#')}">http://localhost:4200/forgot-password?t=${encodeURI('asdjkha98a823eguybo%#')}</a></p>`)).not.toBe(false);
  done();
});