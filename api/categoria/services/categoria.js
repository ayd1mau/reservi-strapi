'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
    sendMail: (_to, _subject, _html) => {
        const nodemailer = require('nodemailer');

        let transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
               user: 'reserviapp@gmail.com',
               pass: 'R3s3rv!@pp'
            }
        });

        const message = {
            from: 'reserviapp@gmail.com', // Sender address
            to: _to,         // List of recipients
            subject: _subject, // Subject line
            html: _html // Plain text body
        };
        let promise = new Promise(function(resolve, reject) {
            // executor (the producing code, "singer")
            transport.sendMail(message, function(err, info) {
                if (err) {
                  console.log(err)
                  reject(false)
                  return err;
                } else {
                    resolve(info)
                  console.log(info);
                  return info;
                }
            });
        
        });
        return promise;
    }
};
