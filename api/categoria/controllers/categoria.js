'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const crypto = require('crypto');
const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const formatError = error => [
    { messages: [{ id: error.id, message: error.message, field: error.field }] },
  ];
const categoriaService = require('../services/categoria')


module.exports = {
    async forgotPassword(ctx) {
        let { email } = ctx.request.body;

        // Check if the provided email is valid or not.
        const isEmail = emailRegExp.test(email);

        if (isEmail) {
            email = email.toLowerCase();
        } else {
            return ctx.badRequest(
                null,
                formatError({
                id: 'Auth.form.error.email.format',
                message: 'Please provide valid email address.',
                })
            );
        }

        const pluginStore = await strapi.store({
            environment: '',
            type: 'plugin',
            name: 'users-permissions',
        });

        // Find the user by email.
        const user = await strapi.query('user', 'users-permissions').findOne({ email });

        // User not found.
        if (!user) {
            return ctx.badRequest(
                null,
                formatError({
                    id: 'Auth.form.error.user.not-exist',
                    message: 'This email does not exist.',
                })
            );
        }

        // Generate random token.
        const resetPasswordToken = crypto.randomBytes(64).toString('hex');        

        // Update the user.
        await strapi.query('user', 'users-permissions').update({ id: user.id }, { resetPasswordToken });

        //Send mail
        categoriaService.sendMail(user.email, 'Reservi - Restablece tu contraseña', `<p>Ingresar al siguiente link para restablecer tu contraseña <a href="http://localhost:4200/forgot-password/${encodeURI(resetPasswordToken)}">http://localhost:4200/forgot-password/${encodeURI(resetPasswordToken)}</a></p>`)

        ctx.send({ "token": resetPasswordToken }); 
    }
};
