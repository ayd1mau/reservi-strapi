#!/bin/bash

sudo apt purge nodejs -y
sudo apt autoremove -y
# Install curl to download latest NodeJS setup
sudo apt-get install curl -y
# Check out https://nodejs.org/ to see what is the latest LTS version
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
# Install PM2
sudo npm install pm2 -g